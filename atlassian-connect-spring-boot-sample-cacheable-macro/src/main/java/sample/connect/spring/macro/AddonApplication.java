package sample.connect.spring.macro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AddonApplication {

    public static void main(String[] args) {
        new SpringApplication(AddonApplication.class).run(args);
    }
}
